// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','ui.bootstrap'])

.run(function($ionicPlatform,$rootScope) {
//$rootScope.host = 'http://localhost/iwater/';
console.log("11")
$rootScope.host = 'http://tapper.co.il/iwater/php/';
$rootScope.Calls = '';
$rootScope.MissionId = 0;

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
  
   .state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  })
  
 

    .state('app.main', {
    url: "/main",
    views: {
      'menuContent': {
        templateUrl: "templates/main.html",
        controller: 'MainCtrl'
      }
    }
  })
  
  
    .state('app.tasks', {
    url: "/tasks",
    views: {
      'menuContent': {
        templateUrl: "templates/tasks.html",
        controller: 'TasksCtrl'
      }
    }
  })
  
  
      .state('app.calls', {
    url: "/calls/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/calls.html",
        controller: 'CallsCtrl'
      }
    }
  })
  
       .state('app.clock', {
    url: "/clock/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/clock.html",
        controller: 'ClockCtrl'
      }
    }
  })
  
    .state('app.newclock', {
    url: "/newclock/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/new_clock.html",
        controller: 'NewClockCtrl'
      }
    }
  })
  
  
      .state('app.task_details', {
    url: "/task-details/:missionid/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/task_details.html",
        controller: 'TaskDetailsCtrl'
      }
    }
  })
  

  //OTHERS
  .state('app.settings', {
    url: "/settings",
    views: {
      'menuContent': {
        templateUrl: "templates/settings.html",
        controller: 'SettingsCtrl'
      }
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'

      }
    }
  })

  
      .state('app.import', {
    url: "/import",
    views: {
      'menuContent': {
        templateUrl: "templates/import.html",
        controller: 'ImportCtrl'
      }
    }
  })
  
        .state('app.export', {
    url: "/export",
    views: {
      'menuContent': {
        templateUrl: "templates/export.html",
        controller: 'ExportCtrl'
      }
    }
  })
  
    .state('app.notifications', {
    url: "/notifications",
    views: {
      'menuContent': {
        templateUrl: "templates/notifications.html",
        controller: 'NotificationsCtrl'
      }
    }
  })
  
    .state('app.readmsg', {
    url: "/readmsg/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/read_notification.html",
        controller: 'ReadNotificationCtrl'
      }
    }
  })
  
    .state('app.details', {
    url: "/details/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/details.html",
        controller: 'DetailsCtrl'
      }
    }
  })
 

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
